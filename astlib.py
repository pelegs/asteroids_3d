# import numpy as np
from ursina import *
from ursina.shaders import unlit_shader
from numpy.random import uniform
from math import sqrt


###########################
#        Constants        #
###########################

G = 1.0E+5
ZERO3D = Vec3(0, 0, 0)
X_ = Vec3(1, 0, 0)
Y_ = Vec3(0, 1, 0)
Z_ = Vec3(0, 0, 1)
power = 1


#########################
#        Classes        #
#########################


class Vec3(Vec3):
    def dot(self, v2):
        return self[0]*v2[0] + self[1]*v2[1] + self[2]*v2[2]

    def norm2(self):
        return self.dot(self)

    def norm(self):
        return sqrt(self.dot(self))

    def unit(self):
        n = self.norm()
        if n == 0:
            return 0
        else:
            return self * 1/n

    def dist2(self, v2):
        dv = Vec3(self - v2)
        return dv.norm2()

    def dist(self, v2):
        return sqrt(self.dist2(v2))


class Object(Entity):
    def __init__(self, model, mass=1.0, pos=ZERO3D, vel=ZERO3D,
                 dir=Z_, **kwargs):
        super().__init__(model=model, position=pos, **kwargs)
        self.mass = mass
        self.vel = vel
        self.dir = dir
        self.camera = camera

    def grav(self, obj, dt=0.01):
        r1 = Vec3(self.position)
        r2 = Vec3(obj.position)
        dr = Vec3(r1 - r2).unit()
        acc = -G * obj.mass / r1.dist2(r2) * dr
        self.vel += acc * dt

    def move(self, dt=0.01):
        self.position += self.vel * dt

    def rotate(self, axis=Z_, theta=0.0):
        pass


###########################
#        Functions        #
###########################


##############################
#        Main "Guard"        #
##############################

if __name__ == "__main__":
    def update():
        for sphere in spheres:
            spaceship.grav(sphere, time.dt)

        if held_keys['w']:
            spaceship.vel += power * Z_
        if held_keys['s']:
            spaceship.vel -= power * Z_
        if held_keys['d']:
            spaceship.vel += power * X_
        if held_keys['a']:
            spaceship.vel -= power * X_
        if held_keys['q']:
            spaceship.vel += power * Y_
        if held_keys['z']:
            spaceship.vel -= power * Y_

        spaceship.move(time.dt)

        x, y, z = spaceship.position
        r1 = Vec3(spaceship.position)
        r2 = Vec3(spheres[0].position)
        d = r1.dist(r2)
        position.text = f"({x:0.1f}, {y:0.1f}. {z:0.1f})"
        distance.text = f"{d:0.1f}"

    app = Ursina()

    window.title = "Asteroids... in 3D?"
    # window.exit_button.visible = True
    # window.fps_counter.enabled = True
    window.color = color.black
    window.size = (800, 800)

    EditorCamera()

    camera.clip_plane_far = 100000
    camera.position = (0, 0, -110)
    camera.fov = 120
    z0 = -100
    vx0 = sqrt(-G/z0)
    spaceship = Object(
        model="cube",
        texture="textures/galvanized_steel.png",
        scale = (0.5, 0.5, 4),
        pos = Vec3(0, 0, z0),
        vel = Vec3(vx0*0.75, 0, 0),
    )
    position = Text(text="(0,0,0)", x=0, y=0.45)
    distance = Text(text="0.0", x=0, y=0.4)

    # N = 20
    # spheres = []
    # Rs = uniform(-10, 10, size=(N, 3))
    # colors = uniform(0.5, 1, size=(N, 4))
    # rads = uniform(10000, 20000, size=(N, 1))
    # for pos, color, rad in zip(Rs, colors, rads):
    #     sphere = Object(model="sphere", color=color, pos=pos, scale=3)
    #     spheres.append(sphere)

    spheres = [Object(model="sphere", color=Vec4(1, 1, 1, 1), scale=8)]
    r1 = Vec3(spaceship.position)
    r2 = Vec3(spheres[0].position)
    print(r1.dist(r2))

    app.run()
